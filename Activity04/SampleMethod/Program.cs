﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

//Name HP Level Mana ATK DEF

namespace SampleMethod
{

    public class Archer
    {

        //public string  Name;
        //public int     HP;
        //public int     Power;

        //private float AtkModifier;

    }

    public class CircleProperties
    {
        private double radius = 1;
        private string color = "red";

        //Setter and Getters
        public void SetRadius(double newValue)
        {
            radius = newValue;
        }

        public void SetColor(string newColor)
        {
            color = newColor;
        }

        public double GetRadius(int multiplier)
        {
            return radius * multiplier;
        }


        public double GetRadius()
    {
        return radius;

    }

        public string GetColor()
    {

        return color;

    }
        
    }

    class Program
    {
        static void Main(string[] args)
        {
            //Archer archerVariable = new Archer();
            //archerVariable.atk

            CircleProperties circleProperties = new CircleProperties();
            Console.WriteLine("Circle Radius = " + circleProperties.GetRadius());
            Console.WriteLine("Circle Color = " + circleProperties.GetColor());

            Console.Write("\nEnter New Radius: ");
            circleProperties.SetRadius(Convert.ToDouble(Console.ReadLine()));
            Console.Write("Enter new Color: ");
            circleProperties.SetColor(Console.ReadLine().ToString());

            Console.WriteLine("\nCircle Radius = " + circleProperties.GetRadius());
            Console.WriteLine("Circle Color = " + circleProperties.GetColor());

            Console.WriteLine(circleProperties.GetRadius(2));

            Console.ReadKey();
            

        }
    }
}
