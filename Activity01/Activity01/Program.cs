﻿using System;
namespace Activity01

{
    class Program
    {

        static void Main(string[] args)
        {

            Console.WriteLine("Weapons that can be input: ");
            Console.WriteLine("1.Sword");
            Console.WriteLine("2.Axe");
            Console.WriteLine("3.Spear");
            Console.WriteLine(" ");
            Console.WriteLine("Input Weapon Type: ");

            string[] items = { "Sword", "Axe", "Spear" };
            string input = Console.ReadLine();
            if (input.Equals(items[0]))

            {
                Console.WriteLine(" ");
                Console.WriteLine(input + " was found in the database");
            }

            else if (input.Equals(items[1]))

            {
                Console.WriteLine(" ");
                Console.WriteLine(input + " was found in the database");
            }

            else if (input.Equals(items[2]))
            {
                Console.WriteLine(" ");
                Console.WriteLine(input + " was found in the database");
            }

            else
            {
                Console.WriteLine("\n");
                Console.WriteLine(input + " was not found in the database");
            }

            Console.ReadKey();
        }

    }

}