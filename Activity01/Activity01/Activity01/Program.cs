﻿using System;

namespace Activity01

{
    class Program

    {
        static void Main(string[] args)

        {

            string itemToSearch = "Sword";
            string[] items = { "Sword", "Axe", "Spear" };

            Console.WriteLine("Input Weapon Type: ");
            string input = Console.ReadLine();


                if (input.Equals(itemToSearch))

                {
                    Console.WriteLine("\n");
                    Console.WriteLine(input + " was found in the database");
                }

                else

                {
                    Console.WriteLine("\n");
                    Console.WriteLine(input + " was not found in the database");
                }

            Console.ReadKey();

        }
    
    }

}
