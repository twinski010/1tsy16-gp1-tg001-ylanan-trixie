﻿using System;
namespace Activity01

{
    class Program
    {

        static void Main(string[] args)
        {

            Console.WriteLine("Choose from the objects that you want to input: ");
            Console.WriteLine("1.Blade");
            Console.WriteLine("2.Scroll");
            Console.WriteLine("3.Quill");
            Console.WriteLine(" ");
            Console.WriteLine("Input: ");

            string input = Console.ReadLine();

            string[] items = { "Blade", "Scroll", "Quill" };
            int i = 0;
            string a = "100";

            bool result1 = int.TryParse(a, out i);
            int j = 0;
            string b = "50";

            bool result2 = int.TryParse(b, out j);
            int k = 0;
            string c = "4000";


            bool result3 = int.TryParse(c, out k);

            if (input.Equals(items[0]))
            {
                Console.WriteLine(" ");
                Console.WriteLine("There are about" + " " + a + " " + input + "s" + " left.");
            }

            else if (input.Equals(items[1]))
            {
                Console.WriteLine(" ");
                Console.WriteLine("There are about" + " " + b + " " + input + "s" + " left.");
            }

            else if (input.Equals(items[2]))
            {
                Console.WriteLine(" ");
                Console.WriteLine("There are about" + " " + c + " " + input + "s" + " left.");
            }

            else
            {
                Console.WriteLine("\n");
                Console.WriteLine(input + " is not in the database");
            }


            Console.ReadKey();
        }
    }
}        