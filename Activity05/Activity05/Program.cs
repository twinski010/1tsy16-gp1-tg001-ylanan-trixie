﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Activity05
{
    public class Monster
    {
        private string Name = "Default Monster";
        private double Level = 1;
        private double HP = 10;
        public string GetName()
        {
            return Name;
        }
        public double GetLevel()
        {
            return Level;
        }
        public double GetHP()
        {
            return HP;
        }

        /////////

        public void SetName(string newName)
        {
            Name = newName;
        }
        public void SetLevel(double newLevel)
        {
            Level = newLevel;
        }
        public void SetHP(double newHP)
        {
            HP = newHP;
        }

    }

    class Program
    {
        static void Main(string[] args)
        {

            int x = 0;
            Console.WriteLine("Enter the number of monsters you want to create: ");

            x = Convert.ToInt32(Console.ReadLine());
            Monster[] monsters = new Monster[x];
            List<string> MonList = new List<string>();

            for (int i = 0; i < x; i++)
            {
                Console.WriteLine(" ");
                Monster monster = new Monster();
                Console.WriteLine("Name: " + monster.GetName());
                Console.WriteLine("Level: " + monster.GetLevel());
                Console.WriteLine("HP: " + monster.GetHP());
            }

            foreach (String names in MonList)

            {
                Console.WriteLine(names);
            }

            Console.WriteLine("\n");
            Console.WriteLine("What do you want to do next?: ");
            Console.WriteLine("[1]Add another monster");
            Console.WriteLine("[2]Remove an existing monster");
            Console.WriteLine("[3]Print the database");
            Console.WriteLine(" ");
            Console.WriteLine("Your Input: ");

            string input = Console.ReadLine();

                if (input.Equals("1"))
                {
                    Console.WriteLine(" ");

                    Monster Monster = new Monster();

                    Console.Write("\nEnter a new name: ");
                    Monster.SetName(Convert.ToString(Console.ReadLine()));

                    Console.Write("Enter a number for the level: ");
                    Monster.SetLevel(Convert.ToDouble(Console.ReadLine().ToString()));

                    Console.Write("Enter a number for the HP: ");
                    Monster.SetHP(Convert.ToDouble(Console.ReadLine().ToString()));

                    Console.WriteLine("\nName: " + Monster.GetName());
                    Console.WriteLine("Level: " + Monster.GetLevel());
                    Console.WriteLine("HP: " + Monster.GetHP());

                    for (int i = 0; i < x; i++)
                    {
                        Console.WriteLine(" ");
                        Monster monster = new Monster();
                        Console.WriteLine("Name: " + monster.GetName());
                        Console.WriteLine("Level: " + monster.GetLevel());
                        Console.WriteLine("HP: " + monster.GetHP());
                    }

                    foreach (String names in MonList)

                    {
                        Console.WriteLine(names);
                    }

                }

                else if (input.Equals("2"))

                {
                    Console.WriteLine(" ");
                    Console.WriteLine("Remove a Name Line #: ");
                    int indexToRemove = Convert.ToInt32(Console.ReadLine());
                    MonList.RemoveAt(indexToRemove - 1);
                    foreach (String names in MonList)
                    {
                        Console.WriteLine(names);
                    }
                }

                else if (input.Equals("3"))
                {
                    for (int i = 0; i < x; i++)
                    {
                        Console.WriteLine(" ");
                        Monster monster = new Monster();
                        Console.WriteLine("Name: " + monster.GetName());
                        Console.WriteLine("Level: " + monster.GetLevel());
                        Console.WriteLine("HP: " + monster.GetHP());
                    }

                    foreach (String names in MonList)

                    {
                        Console.WriteLine(names);
                    }
                }

                else

                {
                    Console.WriteLine("\n");
                    Console.WriteLine("Please input a valid number.");
                }

            Console.ReadKey();
        }
    }
}