﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ResistanceBase
{
    class Program
    {
        static Random random = new Random();
        static int GetNumSpies(int totalPlayers)
        {
            // 5-6 players = 2 spies
            // 7-8 players = 3 spies
            // 9-10 players = 4 spies
            throw new NotImplementedException();
        }

        static int EvaluateRound(string[] players)
        {
            // Return 0 if resistance wins
            // Return 1 if spies win
            throw new NotImplementedException();
        }


        static void PrintPlayers(string[] players)
        {
            throw new NotImplementedException();
        }

        static void AssignRoles(string[] players)
        {

            // Must be dependent on the number of spies
            // Must always reach max # of spies. e.g. if there are 5 players there are always 2 spies
            // Must not exceed the # of spies for that set 
            // e.g if there are 7 players, there are always 3 spies, nothing less nothing more
            PrintPlayers(players);

            throw new NotImplementedException();
        }

        static int GetPlayersForMission(int totalPlayers)
        {
            // 5-6 players = 3 per mission
            // 7-8 players = 4 per mission
            // 9-10 players = 5 per mission
            throw new NotImplementedException();
        }

        static bool ValidateIndex(int[] missionPlayersIndex, int randomIndex)
        {
            for (int j = 0; j < missionPlayersIndex.Length; j++)
            {
                int presentIndex = missionPlayersIndex[j];
                if (presentIndex == randomIndex)
                {
                    return true;
                }
            }
            return false;
        }

        static void Main(string[] args)
        {
            Console.WriteLine("Welcome to the Resistance!");

            string[] type = { "Traitor", "Resistance" };
            int playersNum = 0;
            Console.WriteLine("Enter number of players (5-10): ");
            playersNum = Convert.ToInt32(Console.ReadLine());
            int[] players = new int[playersNum];

            int a = playersNum / 2; //spies
            Random rnd = new Random();
            int com1 = rnd.Next(0, 2);



            for (int y = 0; y < playersNum; y++)
            {

                if (com1.Equals(0))
                {
                    Console.WriteLine(type[1]);
                }

                else if (com1.Equals(1))
                {
                    Console.WriteLine(type[0]);
                }

                Console.WriteLine("Player[" + y + "] :" + com1);
            }

            Console.Read();

            int resistanceScore = 0;
            int spiesScore = 0;
            int roundNum = 1;
            // Begin the missions
            while (resistanceScore < 3 && spiesScore < 3)
            {
                Console.Clear();
                Console.WriteLine("Mission " + roundNum.ToString());
                Console.WriteLine("Resistance: " + resistanceScore.ToString());
                Console.WriteLine("Spies: " + spiesScore.ToString());

                // Select random players based on the players array
                int missionPlayerCount = GetPlayersForMission(playersNum);
                List<int> missionPlayersIndex = new List<int>();
                string[] missionPlayers = new string[missionPlayerCount];
                for (int i = 0; i < missionPlayerCount; i++)
                {
                    int randomIndex = 0;
                    bool alreadyTaken = true;
                    while (alreadyTaken)
                    {
                        randomIndex = random.Next(0, players.Length);
                        alreadyTaken = ValidateIndex(missionPlayersIndex.ToArray(), randomIndex);
                    }
                    missionPlayersIndex.Add(randomIndex);
                    missionPlayers[i] = players[randomIndex];
                }
                // END select random players

                // Play the mission
                int result = EvaluateRound(missionPlayers);
                // Add the result score
                if (result == 0)
                {
                    resistanceScore++;
                }
                else
                {
                    spiesScore++;
                }
                roundNum++;
            }
            // END missions

            if (resistanceScore >= 3)
            {
                Console.WriteLine("Resistance Wins!");
            }
            else if (spiesScore >= 3)
            {
                Console.WriteLine("Spies Wins!");
            }
            Console.ReadKey();

        }
    }
}