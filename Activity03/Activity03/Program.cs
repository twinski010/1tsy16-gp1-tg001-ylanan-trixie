﻿using System;

namespace Activity03
{

    class Program
    {

        static void Main(string[] args)
        {

            Console.WriteLine("Jack 'n Poy ");
            Console.WriteLine(" ");
            Console.WriteLine("Your total money: 1000$");
            Console.WriteLine("Place a bet: ");
            string bet = Console.ReadLine();

            bool accept;

            if ((bet) == "0")
            {
                Console.WriteLine(" ");
                Console.WriteLine("Invalid Bet");
                return;
            }

            else
            {
                Console.WriteLine("\n");
                Console.WriteLine(bet + " is your current bet.");

            }

            Console.Clear();
            Console.WriteLine("Input your choices");
            Console.WriteLine(" ");
            Console.WriteLine("0 = rock");
            Console.WriteLine(" ");
            Console.WriteLine("1 = scissor");
            Console.WriteLine(" ");
            Console.WriteLine("2 = paper");
            Console.WriteLine(" ");

            Console.WriteLine("Input the first number:");
            string num1 = Console.ReadLine();
            Console.WriteLine(" ");
            Console.WriteLine("Input the second number:");
            string num2 = Console.ReadLine();
            Console.WriteLine(" ");
            Console.WriteLine("Input the third number:");
            string num3 = Console.ReadLine();
            Random rnd = new Random();

            int com1 = rnd.Next(0, 3);
            int com2 = rnd.Next(0, 3);
            int com3 = rnd.Next(0, 3);

            Console.Clear();
            Console.WriteLine(" ");
            Console.WriteLine("Your inputs:");
            Console.WriteLine(" ");

            Console.WriteLine(num1);
            Console.WriteLine(num2);
            Console.WriteLine(num3);
            Console.WriteLine(" ");

            Console.WriteLine("The enemy's inputs:");
            Console.WriteLine(" ");
            Console.WriteLine(com1);
            Console.WriteLine(com2);
            Console.WriteLine(com3);

            Console.ReadKey();
        }
    }
}